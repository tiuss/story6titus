from django.test import TestCase, Client
from django.urls import resolve
from .views import index, TambahStatus
from .models import Status
from .forms import FormStatus
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.
class Lab3Test(TestCase):
    '''test apakah LandingPage dapat diakses'''
    def test_LandingPage_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    '''test apakah menggunakan template bernama LandingPage'''
    def test_ladingpage_using_to_do_list_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landingpage.html')

    '''test menggunakan fungsi index yang ada didalam views'''
    def test_LandingPage_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    '''test mode bisa membuat status terbaru'''
    def test_model_can_create_new_Status(self):
        #Creating a new activity
        new_statusnow = Status.objects.create(statusnow='gabaikk tauuu')
        #Retrieving all available activity
        counting_all_available_statusnow = Status.objects.all().count()
        self.assertEqual(counting_all_available_statusnow,1)

    ''' test bisa mengimplementasikan method post '''
    def test_can_save_a_POST_request(self):
        response = self.client.post('/TambahStatus/',data={'statusnow' : 'gabaikk tauuu'})
        counting_all_available_statusnow = Status.objects.all().count()
        self.assertEqual(counting_all_available_statusnow, 1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')
        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('gabaikk tauuu', html_response)

    def test_there_is_form(self):
        response = Client().get('')
        self.assertContains(response, '</form>')
    
    def test_there_is_button_submit(self):
        response = Client().get('')
        self.assertContains(response, '</button>')

    def test_submit_use_add_function(self): 
        found = resolve('/TambahStatus/')
        self.assertEqual(found.func, TambahStatus)

    def test_empty_string_in_models(self):
        form = FormStatus(data={'statusnow': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['statusnow'],
            ["This field is required."]
        )

    def test_representation_string(self):
        status = Status(statusnow = 'cape')
        testString = 'cape'
        self.assertEqual(str(status), testString)

    def test_status_post_failure(self):
        response_post = Client().post('/TambahStatus/', {'statusnow': ''})
        self.assertEqual(response_post.status_code, 302)
    
        response= Client().get('')
        html_response = response.content.decode('utf8')
        self.assertNotIn('tes', html_response)

    def test_there_is_a_word_Hallo(self):
        response = Client().get('')
        self.assertContains(response, 'Hallo')

    def test_there_is_a_word_apa_kabar(self):
        response = Client().get('')
        self.assertContains(response, 'Apa kabar')

class FunctionalTestStory6Titus(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTestStory6Titus, self).setUp()


    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTestStory6Titus, self).tearDown()

    def test_input_status(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/')
        # find the form element
        form = selenium.find_element_by_id('status')

        button = selenium.find_element_by_id('add-btn')

        # # Fill the form with data
        form.send_keys('testing')

        # # submitting the form
        button.click()


